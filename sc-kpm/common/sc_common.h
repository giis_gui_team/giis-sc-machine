#ifndef _sc_common_h_
#define _sc_common_h_

#include "sc_common_prerequest.h"

// Extension interface
sc_result initialize();
sc_result shutdown();

#endif
