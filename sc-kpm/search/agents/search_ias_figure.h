#ifndef SEARCH_IAS_FIGURE_H
#define SEARCH_IAS_FIGURE_H

#include <sc_memory.h>

/*!
 * Function that implement sc-agent to search of all constructions, which are completely isomorphic to given pattern
 */
sc_result agent_search_ias_figure(const sc_event *event, sc_addr arg);

#endif // SEARCH_PATTERN_H