#include "search_ias_figure.h"
#include "search_keynodes.h"
#include "search_utils.h"
#include "search_defines.h"
#include "search.h"

#include <sc_helper.h>
#include <sc_memory_headers.h>

static sc_result build_graphical_object(sc_addr object, sc_addr answer);
static sc_result build_position(sc_addr addr, sc_addr answer);
static sc_result build_figure(sc_addr addr, sc_addr answer);
static sc_result build_value(sc_addr addr, sc_addr answer);
static sc_result build_value_on_rel(sc_addr addr, sc_addr rel, sc_addr answer);

sc_result agent_search_ias_figure(const sc_event *event, sc_addr arg)
{
    sc_addr question, answer;
    sc_addr el;
    sc_result result;
    sc_iterator3 *it1;
    sc_iterator5 *it2;

    if (!sc_memory_get_arc_end(s_default_ctx, arg, &question))
        return SC_RESULT_ERROR_INVALID_PARAMS;

    // check question type
    if (sc_helper_check_arc(s_default_ctx, keynode_question_search_ias_figure, question, sc_type_arc_pos_const_perm) == SC_FALSE)
        return SC_RESULT_ERROR_INVALID_TYPE;

    answer = create_answer_node();

    it1 = sc_iterator3_f_a_a_new(s_default_ctx,
            question,
            sc_type_arc_pos_const_perm,
            0);

    if (sc_iterator3_next(it1) == SC_TRUE) {
        el = sc_iterator3_value(it1, 2);

        appendIntoAnswer(answer, el);

        result = SC_RESULT_OK;
        if (sc_helper_check_arc(s_default_ctx, keynode_ias_graphical_object, el, sc_type_arc_pos_const_perm) != SC_TRUE) {
            sc_iterator5 *it5 = sc_iterator5_f_a_a_a_f_new(s_default_ctx, el,
                    sc_type_arc_common | sc_type_const,
                    0,
                    sc_type_arc_pos_const_perm, keynode_nrel_ias_graphic);

            if (sc_iterator5_next(it5) == SC_TRUE) {
                el = sc_iterator5_value(it5, 2);
            } else {
                result = SC_RESULT_ERROR;
            }

            sc_iterator5_free(it5);

        }

        if (result == SC_RESULT_OK) {
            result = build_graphical_object(el, answer);
        }
    }
    sc_iterator3_free(it1);

    connect_answer_to_question(question, answer);
    finish_question(question);

    return result;
}

static sc_result build_graphical_object(sc_addr object, sc_addr answer) {
    appendIntoAnswer(answer, object);

    build_position(object, answer);
    build_figure(object, answer);

    return SC_RESULT_OK;
}

static sc_result build_position(sc_addr addr, sc_addr answer) {
    sc_addr coordinate;
    sc_addr x, y;

    sc_iterator5 *it5;
    sc_result result = SC_RESULT_OK;

    it5 = sc_iterator5_f_a_a_a_f_new(s_default_ctx,
            addr,
            sc_type_arc_common | sc_type_const,
            0,
            sc_type_arc_pos_const_perm,
            keynode_nrel_ias_coordinates);

    if (sc_iterator5_next(it5) == SC_TRUE) {
        int i;
        for (i = 0; i < 5; ++i) {
            appendIntoAnswer(answer, sc_iterator5_value(it5, i));
        }
        coordinate = sc_iterator5_value(it5, 2);
    } else {
        result = SC_RESULT_ERROR;
    }
    sc_iterator5_free(it5);

    if (result == SC_RESULT_ERROR) {
        return result;
    }

    it5 = sc_iterator5_f_a_a_a_f_new(s_default_ctx,
            coordinate, sc_type_arc_pos_const_perm,
            sc_type_node | sc_type_const, sc_type_arc_pos_const_perm, keynode_rrel_1);

    if (sc_iterator5_next(it5) == SC_TRUE) {
        int i;
        for (i = 0; i < 5; ++i) {
            appendIntoAnswer(answer, sc_iterator5_value(it5, i));
        }
        x = sc_iterator5_value(it5, 2);
    } else {
        result = SC_RESULT_ERROR;
    }
    sc_iterator5_free(it5);

    if (result == SC_RESULT_ERROR) {
        return result;
    }

    it5 = sc_iterator5_f_a_a_a_f_new(s_default_ctx,
            coordinate, sc_type_arc_pos_const_perm,
            sc_type_node | sc_type_const, sc_type_arc_pos_const_perm, keynode_rrel_2);

    if (sc_iterator5_next(it5) == SC_TRUE) {
        int i;
        for (i = 0; i < 5; ++i) {
            appendIntoAnswer(answer, sc_iterator5_value(it5, i));
        }
        y = sc_iterator5_value(it5, 2);
    } else {
        result = SC_RESULT_ERROR;
    }
    sc_iterator5_free(it5);

    if (result == SC_RESULT_ERROR) {
        return result;
    }


    build_value(x, answer);
    build_value(y, answer);
    build_value_on_rel(addr, keynode_nrel_ias_angle, answer);

    return SC_RESULT_OK;
}

static sc_result build_figure(sc_addr addr, sc_addr answer) {
    sc_iterator5 *it5;
    sc_addr shape;
    sc_result result;

    it5 = sc_iterator5_f_a_a_a_f_new(s_default_ctx,
            addr,
            sc_type_arc_common | sc_type_const,
            0,
            sc_type_arc_pos_const_perm,
            keynode_nrel_ias_shape);

    result = SC_RESULT_OK;
    if (sc_iterator5_next(it5) == SC_TRUE) {
        int i;
        for (i = 0; i < 5; ++i) {
            appendIntoAnswer(answer, sc_iterator5_value(it5, i));
        }
        shape = sc_iterator5_value(it5, 2);
    } else {
        result = SC_RESULT_ERROR;
    }
    sc_iterator5_free(it5);

    if (result == SC_RESULT_ERROR) {
        return result;
    }

    sc_iterator3 *it3;

    it3 = sc_iterator3_a_a_f_new(s_default_ctx, 0, sc_type_arc_pos_const_perm, shape);

    while (sc_iterator3_next(it3) == SC_TRUE) {
        sc_addr type;

        type = sc_iterator3_value(it3, 0);
        if (sc_helper_check_arc(s_default_ctx, keynode_ias_type, type, sc_type_arc_pos_const_perm) != SC_TRUE) {
            continue;
        }

        int i;
        for (i = 0; i < 3; ++i) {
            appendIntoAnswer(answer, sc_iterator3_value(it3, i));
        }

        build_value_on_rel(shape, keynode_nrel_ias_radius, answer);
        build_value_on_rel(shape, keynode_nrel_ias_width, answer);
        build_value_on_rel(shape, keynode_nrel_ias_height, answer);
        build_value_on_rel(shape, keynode_nrel_ias_length, answer);

        sc_iterator3 *it1;
        sc_iterator5 *it5;

        it5 = sc_iterator5_f_a_a_a_f_new(s_default_ctx,
                shape,
                sc_type_arc_pos_const_perm, 0, sc_type_arc_pos_const_perm,
                keynode_rrel_ias_base);

        if (sc_iterator5_next(it5) == SC_TRUE) {
            int i;
            for (i = 0; i < 5; ++i) {
                appendIntoAnswer(answer, sc_iterator5_value(it5, i));
            }
        } else {
            result = SC_RESULT_ERROR;
        }
        sc_iterator5_free(it5);

        if (result == SC_RESULT_ERROR)
            break;

        it1 = sc_iterator3_f_a_a_new(s_default_ctx, shape, sc_type_arc_pos_const_perm, 0);
        while (sc_iterator3_next(it1) == SC_TRUE) {
            int i;
            for (i = 0; i < 3; ++i) {
                appendIntoAnswer(answer, sc_iterator3_value(it1, i));
            }

            build_graphical_object(sc_iterator3_value(it3, 2), answer);
        }
        sc_iterator3_free(it1);
    }
    sc_iterator3_free(it3);

    return result;
}

static sc_result build_value(sc_addr addr, sc_addr answer) {
    sc_iterator5 *it5;
    sc_result result;

    it5 = sc_iterator5_f_a_a_a_f_new(s_default_ctx,
            addr,
            sc_type_arc_common | sc_type_const,
            0,
            sc_type_arc_pos_const_perm,
            keynode_nrel_value);

    result = SC_RESULT_OK;
    if (sc_iterator5_next(it5) == SC_TRUE) {
        int i;
        for (i = 0; i < 5; ++i) {
            appendIntoAnswer(answer, sc_iterator5_value(it5, i));
        }
    } else {
        result = SC_RESULT_ERROR;
    }
    sc_iterator5_free(it5);

    return result;
}

static sc_result build_value_on_rel(sc_addr addr, sc_addr rel, sc_addr answer) {
    sc_iterator5 *it5;

    sc_result result = SC_RESULT_OK;

    it5 = sc_iterator5_f_a_a_a_f_new(s_default_ctx,
            addr,
            sc_type_arc_common | sc_type_const,
            0,
            sc_type_arc_pos_const_perm,
            rel);

    if (sc_iterator5_next(it5) == SC_TRUE) {
        int i;
        for (i = 0; i < 5; ++i) {
            appendIntoAnswer(answer, sc_iterator5_value(it5, i));
        }
        build_value(sc_iterator5_value(it5, 2), answer);
    } else {
        result = SC_RESULT_ERROR;
    }
    sc_iterator5_free(it5);

    return result;
}