/*
-----------------------------------------------------------------------------
This source file is part of OSTIS (Open Semantic Technology for Intelligent Systems)
For the latest info, see http://www.ostis.net

Copyright (c) 2010-2014 OSTIS

OSTIS is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

OSTIS is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with OSTIS.  If not, see <http://www.gnu.org/licenses/>.
-----------------------------------------------------------------------------
*/

#ifndef _ui_translator_sc2ias_h_
#define _ui_translator_sc2ias_h_

#include "uiTranslatorFromSc.h"
#include "../../../tools/builder/src/types.h"

/*!
 * \brief Class that translates sc-construction into
 * SCs-code.
 */
class uiSc2IasTranslator : public uiTranslateFromSc
{
public:
    explicit uiSc2IasTranslator();
    virtual ~uiSc2IasTranslator();

    static sc_result ui_translate_sc2ias(const sc_event *event, sc_addr arg);

protected:
    //! @copydoc uiTranslateFromSc::runImpl
    void runImpl();

    //! Resolve system identifier for specified sc-addr
    void resolveSystemIdentifier(const sc_addr &addr, String &idtf);
    static sc_result build_position(const sc_addr &addr, StringStream &ss);
    static sc_result build_figure(const sc_addr &addr, StringStream &ss);
    static sc_result build_graphical_object(sc_addr &object, StringStream &ss);
    static sc_result build_value_on_rel(const sc_addr &addr, const sc_addr &rel, StringStream &ss);
    static sc_result build_value(const sc_addr &addr, StringStream &ss);

protected:
    //! Map of resolved system identifiers
    typedef std::map<sc_addr, String> tSystemIdentifiersMap;
    tSystemIdentifiersMap mSystemIdentifiers;


};

#endif
