/*
-----------------------------------------------------------------------------
This source file is part of OSTIS (Open Semantic Technology for Intelligent Systems)
For the latest info, see http://www.ostis.net

Copyright (c) 2010-2014 OSTIS

OSTIS is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

OSTIS is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with OSTIS.  If not, see <http://www.gnu.org/licenses/>.
-----------------------------------------------------------------------------
*/

#include "uiPrecompiled.h"
#include "uiSc2IasJsonTranslator.h"

#include "uiKeynodes.h"
#include "uiDefines.h"
#include "uiTranslators.h"
#include "uiUtils.h"

// --------------------
uiSc2IasTranslator::uiSc2IasTranslator()
{
}

uiSc2IasTranslator::~uiSc2IasTranslator()
{

}

void uiSc2IasTranslator::runImpl()
{
    sc_result result;

    sc_addr argument;
    sc_addr object;

    sc_iterator5 *it5;

    mOutputData = "{}";

    // get command arguments (keywords)
    it5 = sc_iterator5_a_a_f_a_f_new(s_default_ctx,
            0,
            sc_type_arc_common | sc_type_const,
            mInputConstructionAddr,
            sc_type_arc_pos_const_perm,
            keynode_question_nrel_answer);


    result = SC_RESULT_ERROR;
    if (sc_iterator5_next(it5) == SC_TRUE)
    {
        sc_iterator3 *it3 = sc_iterator3_f_a_a_new(s_default_ctx,
                sc_iterator5_value(it5, 0),
                sc_type_arc_pos_const_perm,
                0);
        while (sc_iterator3_next(it3) == SC_TRUE)
        {
            sc_addr addr = sc_iterator3_value(it3, 2);

            if (sc_helper_check_arc(s_default_ctx, mInputConstructionAddr, addr, sc_type_arc_pos_const_perm) != SC_TRUE)
                continue;

            result = SC_RESULT_OK;
            argument = addr;
        }
        sc_iterator3_free(it3);
    }
    sc_iterator5_free(it5);

    if (result == SC_RESULT_ERROR) {
        return;
    }

    result = SC_RESULT_OK;
    if (sc_helper_check_arc(s_default_ctx, keynode_ias_graphical_object, argument, sc_type_arc_pos_const_perm) != SC_TRUE) {
        sc_iterator5 *it5 = sc_iterator5_f_a_a_a_f_new(s_default_ctx, argument,
                sc_type_arc_common | sc_type_const,
                0,
                sc_type_arc_pos_const_perm, keynode_nrel_ias_graphic);

        if (sc_iterator5_next(it5) == SC_TRUE) {
            object = sc_iterator5_value(it5, 2);
        } else {
            result = SC_RESULT_ERROR;
        }

        sc_iterator5_free(it5);

    } else {
        object = argument;
    }

    if (result == SC_RESULT_OK) {
        StringStream ss;
        if (build_graphical_object(object, ss))
            mOutputData = ss.str();
    }

}

sc_result uiSc2IasTranslator::build_graphical_object(sc_addr &object, StringStream &ss) {

    ss << "{";

    build_position(object, ss);

    ss << ", ";

    build_figure(object, ss);

    ss << "}";

    return SC_RESULT_OK;
}

void uiSc2IasTranslator::resolveSystemIdentifier(const sc_addr &addr, String &idtf)
{
    tSystemIdentifiersMap::iterator it = mSystemIdentifiers.find(addr);
    if (it != mSystemIdentifiers.end())
    {
        idtf = it->second;
        return;
    }

    ui_translate_resolve_system_identifier(addr, idtf);
    mSystemIdentifiers[addr] = idtf;
}


// -------------------------------------------------------
sc_result uiSc2IasTranslator::ui_translate_sc2ias(const sc_event *event, sc_addr arg)
{
    sc_addr cmd_addr, input_addr, format_addr;

    if (sc_memory_get_arc_end(s_default_ctx, arg, &cmd_addr) != SC_RESULT_OK)
        return SC_RESULT_ERROR;

    if (ui_check_cmd_type(cmd_addr, keynode_command_translate_from_sc) != SC_RESULT_OK)
        return SC_RESULT_ERROR;

    if (ui_translate_command_resolve_arguments(cmd_addr, &format_addr, &input_addr) != SC_RESULT_OK)
        return SC_RESULT_ERROR;

    if (format_addr == keynode_format_ias_json)
    {
        uiSc2IasTranslator translator;
        translator.translate(input_addr, format_addr);
    }

    return SC_RESULT_OK;
}

sc_result uiSc2IasTranslator::build_position(const sc_addr &addr, StringStream &ss) {
    sc_addr coordinate;
    sc_addr x, y;

    sc_iterator5 *it5;
    sc_result result = SC_RESULT_OK;

    it5 = sc_iterator5_f_a_a_a_f_new(s_default_ctx,
            addr,
            sc_type_arc_common | sc_type_const,
            0,
            sc_type_arc_pos_const_perm,
            keynode_nrel_ias_coordinates);

    if (sc_iterator5_next(it5) == SC_TRUE) {
        coordinate = sc_iterator5_value(it5, 2);
    } else {
        result = SC_RESULT_ERROR;
    }
    sc_iterator5_free(it5);

    if (result == SC_RESULT_ERROR) {
        return result;
    }

    it5 = sc_iterator5_f_a_a_a_f_new(s_default_ctx,
            coordinate, sc_type_arc_pos_const_perm,
            sc_type_node | sc_type_const, sc_type_arc_pos_const_perm, keynode_rrel_1);

    if (sc_iterator5_next(it5) == SC_TRUE) {
        x = sc_iterator5_value(it5, 2);
    } else {
        result = SC_RESULT_ERROR;
    }
    sc_iterator5_free(it5);

    if (result == SC_RESULT_ERROR) {
        return result;
    }

    it5 = sc_iterator5_f_a_a_a_f_new(s_default_ctx,
            coordinate, sc_type_arc_pos_const_perm,
            sc_type_node | sc_type_const, sc_type_arc_pos_const_perm, keynode_rrel_2);

    if (sc_iterator5_next(it5) == SC_TRUE) {
        y = sc_iterator5_value(it5, 2);
    } else {
        result = SC_RESULT_ERROR;
    }
    sc_iterator5_free(it5);

    if (result == SC_RESULT_ERROR) {
        return result;
    }

    ss << "\"coordinates\" : [ ";

    build_value(x, ss);
    ss << ", ";

    build_value(y, ss);

    ss << "], ";

    ss << "\"angle\" : ";
    build_value_on_rel(addr, keynode_nrel_ias_angle, ss);

    return SC_RESULT_OK;
}

sc_result uiSc2IasTranslator::build_figure(const sc_addr &addr, StringStream &ss) {
    sc_iterator5 *it5;
    sc_addr shape;
    sc_result result;

    it5 = sc_iterator5_f_a_a_a_f_new(s_default_ctx,
            addr,
            sc_type_arc_common | sc_type_const,
            0,
            sc_type_arc_pos_const_perm,
            keynode_nrel_ias_shape);

    result = SC_RESULT_OK;
    if (sc_iterator5_next(it5) == SC_TRUE) {
        shape = sc_iterator5_value(it5, 2);
    } else {
        result = SC_RESULT_ERROR;
    }
    sc_iterator5_free(it5);

    if (result == SC_RESULT_ERROR) {
        return result;
    }

    sc_iterator3 *it3;
    bool found;

    it3 = sc_iterator3_a_a_f_new(s_default_ctx, 0, sc_type_arc_pos_const_perm, shape);

    found = false;
    while (sc_iterator3_next(it3) == SC_TRUE && !found) {
        sc_addr type;

        type = sc_iterator3_value(it3, 0);

        if (type == keynode_ias_circle) {
            found = true;

            ss << "\"type\" : \"circle\", ";

            ss << "\"radius\" : ";
            build_value_on_rel(shape, keynode_nrel_ias_radius, ss);
        } else if (type == keynode_ias_rectangle) {
            found = true;
            ss << "\"type\" : \"rectangle\", ";

            ss << "\"width\" : ";
            build_value_on_rel(shape, keynode_nrel_ias_width, ss);
            ss << ", ";

            ss << "\"height\" : ";
            build_value_on_rel(shape, keynode_nrel_ias_height, ss);
        } else if (type == keynode_ias_line) {
            found = true;
            ss << "\"type\" : \"line\", ";

            ss << "\"length\" : ";
            build_value_on_rel(shape, keynode_nrel_ias_length, ss);
        } else if (type == keynode_ias_group) {
            sc_iterator3 *it1;
            sc_iterator5 *it5;
            sc_addr base;
            sc_addr obj;
            bool first = true;

            ss << "\"type\" : \"group\", ";

            it5 = sc_iterator5_f_a_a_a_f_new(s_default_ctx,
                    shape,
                    sc_type_arc_pos_const_perm, 0, sc_type_arc_pos_const_perm,
                    keynode_rrel_ias_base);

            if (sc_iterator5_next(it5) == SC_TRUE) {
                base = sc_iterator5_value(it5, 2);
                ss << "\"base\" : ";
                build_graphical_object(base, ss);
                ss << ", ";
            } else {
                result = SC_RESULT_ERROR;
            }
            sc_iterator5_free(it5);

            if (result == SC_RESULT_ERROR)
                break;

            ss << "\"others\": [ ";

            it1 = sc_iterator3_f_a_a_new(s_default_ctx, shape, sc_type_arc_pos_const_perm, 0);
            while (sc_iterator3_next(it1) == SC_TRUE) {
                obj = sc_iterator3_value(it1, 2);
                if (base != obj) {
                    if (first) {
                        first = false;
                    } else {
                        ss << ", ";
                    }
                    build_graphical_object(obj, ss);
                }
            }
            sc_iterator3_free(it1);

            ss << "]";
        }
    }
    sc_iterator3_free(it3);

    return result;
}

sc_result uiSc2IasTranslator::build_value(const sc_addr &addr, StringStream &ss) {
    sc_iterator5 *it5;
    sc_addr content;
    sc_result result;

    sc_stream *stream;
    sc_uint32 size;
    sc_uint32 read_bytes = 0;
    sc_char buffer[32];

    String text;


    it5 = sc_iterator5_f_a_a_a_f_new(s_default_ctx,
            addr,
            sc_type_arc_common | sc_type_const,
            0,
            sc_type_arc_pos_const_perm,
            keynode_nrel_value);

    result = SC_RESULT_OK;
    if (sc_iterator5_next(it5) == SC_TRUE) {
        content = sc_iterator5_value(it5, 2);
    } else {
        result = SC_RESULT_ERROR;
    }
    sc_iterator5_free(it5);

    if (result == SC_RESULT_ERROR)
        return result;

    text = "";

    sc_memory_get_link_content(s_default_ctx, content, &stream);
    sc_stream_get_length(stream, &size);

    while (sc_stream_eof(stream) == SC_FALSE)
    {
        sc_stream_read_data(stream, buffer, 32, &read_bytes);
        text.append(buffer, read_bytes);
    }
    sc_stream_free(stream);

    ss << text;

    return result;
}

sc_result uiSc2IasTranslator::build_value_on_rel(const sc_addr &addr, const sc_addr &rel, StringStream &ss) {
    sc_iterator5 *it5;

    it5 = sc_iterator5_f_a_a_a_f_new(s_default_ctx,
            addr,
            sc_type_arc_common | sc_type_const,
            0,
            sc_type_arc_pos_const_perm,
            rel);

    if (sc_iterator5_next(it5) == SC_TRUE) {
        sc_addr radius;

        radius = sc_iterator5_value(it5, 2);

        build_value(radius, ss);

    }
    return SC_RESULT_OK;
}
